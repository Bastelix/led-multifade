#include "PCA9685LEDs.h"

const uint8_t PCA9685LEDs::LED0_ON_L = 0x06;

PCA9685LEDs::PCA9685LEDs(uint8_t startAddress) : startAddress(startAddress) {
}

void PCA9685LEDs::begin(uint16_t ledCount) {
    Wire.begin();
    delay(10);

    // for each 16 LEDs we need one PCA, the address are incremented start with startAddress (default 0x40)
    uint8_t endAddress = startAddress + (ledCount / 16);

    for (uint8_t address = startAddress; address <= endAddress; ++address) {
        // Set frequency to 1000Hz, enable internal clock, AI, allcall
        Wire.beginTransmission(address);
        Wire.write(PCA9685_MODE1);
        Wire.write(0xB1); // sleep = true
        Wire.endTransmission();

        Wire.beginTransmission(address);
        Wire.write(0xFE);
        Wire.write(5); // 1000Hz
        Wire.endTransmission();

        Wire.beginTransmission(address);
        Wire.write(PCA9685_MODE1);
        Wire.write(0xA1); // sleep = false
        Wire.endTransmission();
    }

    allOff();
}

void PCA9685LEDs::allOff() {
    // turn off all LEDs on all modules (use call all address)
    Wire.beginTransmission(0x70);
    Wire.write(0xFD);
    Wire.write(0x10);
    Wire.endTransmission();
}

void PCA9685LEDs::allOn() {
    // FIXME only work one time at the moment
    // turn on all LEDs on all modules (use call all address)
    Wire.beginTransmission(0x70);
    //Wire.write(0xFA);
    //Wire.write(0x00);
    Wire.write(0xFB);
    Wire.write(0x10);
    Wire.endTransmission();
}

void PCA9685LEDs::writePwm(uint16_t index, pwm_t pwm) {
  uint8_t buffer[3];
  buffer[0] = LED0_ON_L + 4 * pinFromIndex(index);
  buffer[1] = pwm;
  buffer[2] = pwm >> 8;

  Wire.beginTransmission(addressFromIndex(index));

  Wire.write(buffer[0]);
  Wire.write(buffer[1]);
  Wire.write(buffer[2]);
  Wire.write(0);
  Wire.write(0);

  Wire.endTransmission();
}