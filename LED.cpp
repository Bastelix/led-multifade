#include "LED.h"

const uint32_t LED::PWM_MAX = 4096;
const uint32_t LED::IN_MAX = 'z' - 'a';
const pwm_t LED::STEP_SIZE = 100;
const bool LED::INVERT_PIN = false;

LED::LED() : driver(NULL), pin(0), pwm(0), state(0x02), sequence(NULL) {
}

void LED::update() {
  if (!READ_BIT_AT(state, ACTIVE_BIT)) {
    return;
  }

  // count pwm value up or down, depending on rising flag
  if (READ_BIT_AT(state, SEQUENCE_BIT) && sequence != NULL) {
    if (*sequence == 0) {
      // Serial.println("Found \\0 terminator");
      pwm = 0;
      sequence = NULL; // delete sequence ptr
      
      state = 0x02;
    } else {
      // Serial.print("Next sequence char is ");
      // Serial.println(*sequence);
      pwm = map((*sequence) - 'a');

      // Serial.print("Calculated pwm value is ");
      // Serial.println(pwm);
      ++sequence; // set ptr to next
    }
  } else if (READ_BIT_AT(state, PWM_RISING_BIT)) {
    pwm += STEP_SIZE;
  } else {
    pwm -= STEP_SIZE;
  }

  // Ensure pwm is inside the bounds. Only required if pwm_t is signed.
  if (pwm > PWM_MAX) {
    pwm = PWM_MAX;
  } else if (pwm < 0) {
    pwm = 0;
  }
  writePwm();

  if (!READ_BIT_AT(state, SEQUENCE_BIT)) {
    // reset rising and active flag on pwm bounds
    if (pwm == PWM_MAX) {
      UNSET_BIT_AT(state, PWM_RISING_BIT);
    } else if (pwm == 0) {
      state = 0x02;
    }
  }
}

void LED::set(pwm_t value) {
  pwm = value;
  // set active if pwm value > 0
  if (pwm > 0) {
    SET_BIT_AT(state, ACTIVE_BIT);
  } else {
    UNSET_BIT_AT(state, ACTIVE_BIT);
  }
  writePwm();
}

void LED::writePwm() {
  driver->writePwm(pin, pwm);
}

pwm_t LED::map(pwm_t value) {
  return ((uint32_t) value) * PWM_MAX / IN_MAX;
}
