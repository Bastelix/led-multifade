#include "PCA9685LEDs.h"
#include "LED.h"

#define DEBUG_OUTPUT 0

// Wahrscheinlichkeit, dass eine LED eingeschaltet wird, wenn weniger als MAX_ACTIVE LEDs leuchten
// Ein Wert von 2 bedeutet, dass zu 50% eine LED pro loop()-Durchlauf gesetzt wird, 
// Werte > 2 verringert die Wahrscheinlichkeit pro loop()-Durchlauf eine LED eingeschaltet wird
// Bei 6 LEDs ist der Lichteffekt besser wenn der wert >= 20 ist
const uint8_t ENABLE_LED_RAND_MAX = 20;
// Wartezeit in ms zwischen jedem loop()-Durchlauf
const unsigned long LOOP_DELAY = 10;
// Anzahl der maximal gleichzeitk leuchtenden LEDs
const uint8_t MAX_ACTIVE = 3;
// Anzahl der LEDs
const uint8_t LEDS = 6;

PCA9685LEDs pca;

LED leds[LEDS];

void setup() {
  #if DEBUG_OUTPUT
    Serial.begin(9600);
    Serial.println(F("Start Blinkenlights"));
  #endif

  pca.begin(LEDS);
  for (uint8_t i = 0; i < LEDS; ++i) {
    leds[i].begin(&pca, i);
  }

  // Verdrahtungstest: Lässt jede LED für eine Sekunde voll aufleuchten
  for (uint8_t i = 0; i < LEDS; ++i) {
    leds[i].set(4096);
    delay(1000);
    leds[i].set(0);
  }
  delay(2000);

  // for (uint8_t i = 0; i < LEDS; ++i) {
  //   leds[i].set(4096);
  // }
  // delay(5000);
  // pca.allOff();
  // delay(500);
}


void loop() {
  uint8_t totalActive = 0;

  // LED state aktualisieren und zählen welche LEDs noch aktiv sind
  for (uint8_t i = 0; i < LEDS; ++i) {
    // LED PWM aktualisieren
    leds[i].update();
    // aktiv counter aktualisieren
    if (leds[i].isActive()) {
      ++totalActive;
      Serial.print("LED ");
      Serial.print(i);
      Serial.println(" is active");
    }
  }

  // Wenn weniger LEDs aktiv sind als aktiv sein dürfen
  if (totalActive < MAX_ACTIVE) {
    // und eine LED eingeschaltet werden soll
    if(random(ENABLE_LED_RAND_MAX) == 0) {
      // zufällig die LED bestimmen
      int nextLed = random(LEDS);

      #if DEBUG_OUTPUT
        Serial.print(F("Set LED "));
        Serial.println(nextLed);
      #endif

      switch (random(7)) {
        case 0:
          #if DEBUG_OUTPUT
            Serial.println(F(" to sequence 0"));
          #endif
          leds[nextLed].setSequence("jklmnopqrstuvwxyzxwvutsrqponmlkj");
          break;
        case 1:
          #if DEBUG_OUTPUT
            Serial.println(F(" to sequence 1"));
          #endif
          leds[nextLed].setSequence("cdefghijklmnopqrrqponmlkjihgfedc");
          break;
        case 2:
          #if DEBUG_OUTPUT
            Serial.println(F(" to sequence 2"));
          #endif
          leds[nextLed].setSequence("hijklmnopqrstuvwxvutsrqponmlkjih");
          break;
        case 3:
          #if DEBUG_OUTPUT
            Serial.println(F(" to sequence 3"));
          #endif
          leds[nextLed].setSequence("ffghijkklmmnoppqqpponmmlkkjighff");
          break;
        case 4:
          #if DEBUG_OUTPUT
            Serial.println(F(" to sequence 4"));
          #endif
          leds[nextLed].setSequence("qqqpooonmllkkjihhijkkllmnooopqqq");
          break;
        case 5:
          #if DEBUG_OUTPUT
            Serial.println(F(" to sequence 5"));
          #endif
          leds[nextLed].setSequence("fgghhiijkklmmmnoonmmmlkkjiihhggf");
          break;
        default:
          #if DEBUG_OUTPUT
            Serial.println(F(" to full fade"));
          #endif
          leds[nextLed].setActive();
      }
    }
  }

  // Kurze pause, für die Leucht-Effekte
  delay(LOOP_DELAY);
}
