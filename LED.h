#ifndef __LED_H__
#define __LED_H__ 1

#include <Arduino.h>
#include <Wire.h>

#include "PCA9685LEDs.h"

/**
 * Set bit at position pos in variable var to 1
 */
#define SET_BIT_AT(var, pos) var |= 1 << pos

/**
 * Set bit at position pos in variable var to 0
 */
#define UNSET_BIT_AT(var, pos) var &= ~(1 << pos)

/**
 * Read bit at position pos in variable var
 */
#define READ_BIT_AT(var, pos)  (var & ( 1 << pos )) >> pos

#ifndef _PWM_T
#define _PWM_T
  // Ardino PWM max value is normally 255, if you use another µC oder IC for PWM you might adjust the pwm_t type.
  // For the PCA9685 we use a signed type because we increment/decrement with values > 1
  typedef int16_t pwm_t;
#endif // _PWM_T

// Define the bit position for each flag used by the state variable
#define ACTIVE_BIT     0
#define PWM_RISING_BIT 1
#define SEQUENCE_BIT   2

/**
 * Represent a single LED (or serial LEDs) on on PWM pin.
 */
class LED {
  private:
    static const uint32_t   PWM_MAX;
    static const uint32_t   IN_MAX;
    static const pwm_t   STEP_SIZE;
    static const bool    INVERT_PIN;

    PCA9685LEDs* driver;
    uint16_t pin;

    pwm_t pwm;
    // Using each bit in this uint8_t as a boolean flag. 
    // At bit 0 we maintaine active state and at bit 1 we maintain rising/falling LED state
    // save one byte per LED instance of global memory if not use two bool variables for this
    uint8_t state;
    // String used as a sequence of chars where 'a' represents a pwm value of 0 and 'z' represents a pwm value of PWM_MAX .
    // Every char betweeen 'a' and 'z' represents some pwm value greater than 0 and less than PWM_MAX
    // See https://www.elektronik-kompendium.de/sites/raspberry-pi/2701061.htm if you want to know where the idear is from ;)
    const char* sequence;

  public:
    LED();

    inline void begin(PCA9685LEDs* driver, uint16_t pin) {
      this->driver = driver;
      this->pin = pin;
    }

    /**
     * @return true wenn die LED aktiv ist; sonst false
     */
    inline bool isActive() {
      return READ_BIT_AT(state, ACTIVE_BIT);
    }

    /**
     * Setzt das active Flag für diese LED
     */
    inline void setActive() {
      SET_BIT_AT(state, ACTIVE_BIT);
    }

    /**
     * Setzt den PWM-Wert auf value
     * @param value the new PWM value
     */
    void set(pwm_t value);

    /**
     * Setzt eine Sequenz an Hellingkeitswerten als String.
     * (Das char array muss \0 terminiert sein)
     */
    inline void setSequence(const char* ptr) {
      state = 0x05;
      sequence = ptr;
    }

    /**
     * Update the internal LED state if this LED is active.
     */
    void update();
  
  protected:
    void writePwm();
    pwm_t map(pwm_t value);
};

#endif // __LED_H__
