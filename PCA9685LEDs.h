#ifndef __PCA9685LEDs_H__
#define __PCA9685LEDs_H__

#include <Arduino.h>
#include <Wire.h>

#ifndef _PWM_T
#define _PWM_T
  // Ardino PWM max value is normally 255, if you use another µC oder IC for PWM you might adjust the pwm_t type.
  // For the PCA9685 we use a signed type because we increment/decrement with values > 1
  typedef int16_t pwm_t;
#endif // _PWM_T

class PCA9685LEDs;

#include "LED.h"

#define PCA9685_MODE1 0x00

class PCA9685LEDs {
  private:
    static const uint8_t LED0_ON_L;

    uint8_t startAddress;

  public:
    PCA9685LEDs(uint8_t startAddress = 0x40);

    void begin(uint16_t ledCount);

    void writePwm(uint16_t index, pwm_t pwm);

    void allOff();

    void allOn();

  private:
    inline uint8_t addressFromIndex(uint16_t index) {
        return startAddress + (index / 16);
    }

    inline uint8_t pinFromIndex(uint16_t index) {
      return index % 16;
    }

};

#endif // __PCA9685LEDs_H__